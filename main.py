import logging
# we need to configure the logger in the top-level script before anything is printed out, the basicConfig method is usually good enough
logging.basicConfig(level=logging.DEBUG)
# since these modules will log data when they are imported, we need to configure the logger before we import them
# otherwise we will miss their logs
from test_package.test_class import TestClass
from test_package import test_module


logger = logging.getLogger(__name__)

logger.info('Starting main script')
test_module.log('Test Module')
test = TestClass()
test.log('Test Class')
