import logging

# use this logger for anything inside this file that is not a class
logger = logging.getLogger(__name__)

logger.debug('Running test_package/test_module.py')


def log(message):
    logger.debug('Logging message')
    logger.info(message)
