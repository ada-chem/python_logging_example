import logging

# use this logger for anything inside this file that is not a class
logger = logging.getLogger(__name__)

logger.debug('Running test_package/test_class.py')


class TestClass:
    def __init__(self):
        # create a child instance of the module's logger for class instances to use
        self.logger = logger.getChild(self.__class__.__name__)

    def log(self, message):
        # use the class instance when logging inside of classes
        self.logger.debug('Logging message')
        self.logger.info(message)
